package us.siglerdev.finalproject.objects;

import us.siglerdev.finalproject.utils.ArcadeHelper;

import javax.swing.*;

public class ArcadeJLabel extends JLabel {

    public ArcadeJLabel(String text) {
        super(text);
        this.setFont(ArcadeHelper.LABEL_FONT);
    }

    public ArcadeJLabel(){
        super();
    }

}
