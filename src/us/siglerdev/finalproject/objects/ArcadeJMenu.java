package us.siglerdev.finalproject.objects;

import us.siglerdev.finalproject.utils.ArcadeHelper;

import javax.swing.*;

public class ArcadeJMenu extends JMenu {

    public ArcadeJMenu(String menuButtonName){
        super(menuButtonName);
        this.setFont(ArcadeHelper.MENU_BUTTON_FONT);
    }

}
