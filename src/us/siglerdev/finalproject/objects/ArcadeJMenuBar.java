package us.siglerdev.finalproject.objects;

import us.siglerdev.finalproject.Arcade;
import us.siglerdev.finalproject.buttons.AboutMenuButton;
import us.siglerdev.finalproject.buttons.ExitMenuButton;
import us.siglerdev.finalproject.buttons.HomeMenuButton;
import us.siglerdev.finalproject.buttons.PacmanMenuButton;
import us.siglerdev.finalproject.buttons.ResizeMenuButton;
import us.siglerdev.finalproject.buttons.StackerMenuButton;

import javax.swing.*;

public class ArcadeJMenuBar extends JMenuBar{
    private final Arcade arcade;
    public ArcadeJMenuBar(Arcade arcade){
        super();
        this.arcade = arcade;
        initMenus();
    }

    /**
     * Initialize all the menu bars
     */
    private void initMenus(){
        ArcadeJMenu file = new ArcadeJMenu("File");
        ArcadeJMenu games = new ArcadeJMenu("Games");
        ArcadeJMenu resize = new ArcadeJMenu("Resize");
        ExitMenuButton exit = new ExitMenuButton(arcade);
        AboutMenuButton about = new AboutMenuButton(arcade);
        HomeMenuButton homeMenuButton = new HomeMenuButton(arcade);
        PacmanMenuButton pacmanMenuButton = new PacmanMenuButton(arcade);
        StackerMenuButton stackerMenuButton = new StackerMenuButton(arcade);


        file.add(about);
        file.add(exit);
        games.add(homeMenuButton);
        games.add(pacmanMenuButton);
        games.add(stackerMenuButton);
        resize.add(new ResizeMenuButton(arcade, .5, "50%"));
        resize.add(new ResizeMenuButton(arcade, .8, "80%"));
        resize.add(new ResizeMenuButton(arcade, .9, "90%"));
        resize.add(new ResizeMenuButton(arcade, 1, "100%"));
        resize.add(new ResizeMenuButton(arcade, 1.15, "115%"));
        resize.add(new ResizeMenuButton(arcade, 1.25, "125%"));

        this.add(file);
        this.add(games);
        this.add(resize);

    }
}
