package us.siglerdev.finalproject.objects;

import us.siglerdev.finalproject.Arcade;
import us.siglerdev.finalproject.utils.ArcadeHelper;

import javax.swing.*;

public abstract class ArcadeJMenuItem extends JMenuItem{
    protected final Arcade arcade;
    protected ArcadeJMenuItem(Arcade arcade, String text) {
        super(text);
        this.arcade = arcade;
        this.setFont(ArcadeHelper.MENU_BUTTON_FONT);
    }

    public abstract void executeMenuItem();

}
