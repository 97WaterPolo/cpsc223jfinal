package us.siglerdev.finalproject.utils;

import us.siglerdev.finalproject.objects.ArcadeJMenu;

import java.awt.*;

import java.util.ArrayList;
import java.util.List;

public class ArcadeHelper {

    /* All of my static and final variables */
    public static final Font MENU_BUTTON_FONT = new Font("Courier Bold Italic", Font.PLAIN, 24);
    public static final Font LABEL_FONT = new Font("Monotype Corsiva", Font.PLAIN, 30);
    public static final int ROWS = 20;
    public static final int COLUMNS = 20;
    public static final int STACKER_DELAY = 500;
    public static final double STACKER_MULTIPLIER = 0.85;
    public static final int STACKER_MAJOR_PRIZE = 10000;
    public static final int STACKER_MINOR_PRIZE = 2000;
    public static final int HOME_WIDTH = 1200;
    public static final int HOME_HEIGHT = 600;
    public static final int STACKER_WIDTH = 500;
    public static final int STACKER_HEIGHT = 1000;
    public static final int PACMAN_WIDTH = 1000;
    public static final int PACMAN_HEIGHT = 1000;


    /**
     * Get all the components in a given container
     * @param c the container
     * @return a list of the Components
     */
    public static java.util.List<Component> getAllComponents(final Container c) {
        Component[] comps = c.getComponents();
        List<Component> compList = new ArrayList<>();
        for (Component comp : comps) {
            compList.add(comp);
            if (comp instanceof Container)
                compList.addAll(getAllComponents((Container) comp));
            if (comp instanceof ArcadeJMenu)
                for (Component menuItem : ((ArcadeJMenu)comp).getMenuComponents())
                    compList.add(menuItem);
        }
        return compList;
    }

    private static double multiplier = 1.0;

    /**
     * Set the multiplier for scaling
     * @param value the double value to be replaces
     */
    public static void setMultiplier(double value){
        multiplier = value;
    }

    /**
     * The scales instance for image scaling
     * @return The int value for scaledInstance()
     */
    public static int getScaledInstance(){
        return (int)(multiplier * 35);
    }

}
