package us.siglerdev.finalproject.buttons;

import us.siglerdev.finalproject.Arcade;
import us.siglerdev.finalproject.objects.ArcadeJMenuItem;

public class ExitMenuButton extends ArcadeJMenuItem {

    public ExitMenuButton(Arcade arcade) {
        super(arcade, "Exit");
    }

    @Override
    public void executeMenuItem() {
        arcade.dispose();
        System.exit(0);
    }
}
