package us.siglerdev.finalproject.buttons;

import javax.swing.JOptionPane;
import us.siglerdev.finalproject.Arcade;
import us.siglerdev.finalproject.objects.ArcadeJMenuItem;
import us.siglerdev.finalproject.utils.ArcadeHelper;

public class ResizeMenuButton extends ArcadeJMenuItem {

    private final double value;
    public ResizeMenuButton(Arcade arcade, double value, String name) {
        super(arcade, name);
        this.value = value;
    }

    @Override
    public void executeMenuItem() {
        ArcadeHelper.setMultiplier(value);
        JOptionPane.showMessageDialog(null, "Changing scaling to: " + (value*100) + "%",
                "Resizing", JOptionPane.INFORMATION_MESSAGE);
    }
}
