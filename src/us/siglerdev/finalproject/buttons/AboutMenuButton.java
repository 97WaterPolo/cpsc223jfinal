package us.siglerdev.finalproject.buttons;

import us.siglerdev.finalproject.Arcade;
import us.siglerdev.finalproject.objects.ArcadeJMenuItem;

import javax.swing.*;

public class AboutMenuButton extends ArcadeJMenuItem {
    public AboutMenuButton(Arcade arcade) {
        super(arcade, "About");
    }

    @Override
    public void executeMenuItem() {
        String msg = "This is a simple Arcade program which currently has Stacker and Pacman\n";
        msg += "Created by Alexander Sigler\n";
        msg += "Made for CPSC 223J Final project!";
        JOptionPane.showMessageDialog(null, msg, "About", JOptionPane.INFORMATION_MESSAGE);
    }
}
