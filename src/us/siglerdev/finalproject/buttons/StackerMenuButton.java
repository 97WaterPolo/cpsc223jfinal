package us.siglerdev.finalproject.buttons;

import us.siglerdev.finalproject.Arcade;
import us.siglerdev.finalproject.objects.ArcadeJMenuItem;

import javax.swing.*;

public class StackerMenuButton extends ArcadeJMenuItem {
    public StackerMenuButton(Arcade arcade) {
        super(arcade, "Stacker");
    }

    @Override
    public void executeMenuItem() {
        JOptionPane.showMessageDialog(null, "Starting Stacker",
                "Stacker", JOptionPane.INFORMATION_MESSAGE);
        this.arcade.startStacker();
    }
}
