package us.siglerdev.finalproject.buttons;

import us.siglerdev.finalproject.Arcade;
import us.siglerdev.finalproject.objects.ArcadeJMenuItem;

import javax.swing.*;

public class PacmanMenuButton extends ArcadeJMenuItem {
    public PacmanMenuButton(Arcade arcade) {
        super(arcade, "Pacman");
    }

    @Override
    public void executeMenuItem() {
        JOptionPane.showMessageDialog(null, "Starting Pacman",
                "Pacman", JOptionPane.INFORMATION_MESSAGE);
        arcade.startPacman();
    }
}
