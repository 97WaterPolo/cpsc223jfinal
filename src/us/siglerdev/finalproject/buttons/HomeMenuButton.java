package us.siglerdev.finalproject.buttons;

import us.siglerdev.finalproject.Arcade;
import us.siglerdev.finalproject.objects.ArcadeJMenuItem;

public class HomeMenuButton extends ArcadeJMenuItem {
    public HomeMenuButton(Arcade arcade) {
        super(arcade, "Home");
    }

    @Override
    public void executeMenuItem() {
        arcade.endGame();
    }
}
