package us.siglerdev.finalproject.games.stacker;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;
import javax.swing.JOptionPane;
import us.siglerdev.finalproject.Arcade;
import us.siglerdev.finalproject.games.GameMode;

import javax.swing.JPanel;
import java.awt.*;
import us.siglerdev.finalproject.utils.ArcadeHelper;

public class Stacker extends JPanel {

    private final GameMode GAMEMODE = GameMode.STACKER;
    private final Color BACK_COLOR = new Color(0,0, 78);
    private final Color BLOCK_COLOR = new Color(0, 198, 255);
    private final int ROWS = 15, COLUMNS = 7;
    private final JPanel[][] stackerBoard;

    private final Timer timer;
    private StackerRunner runner;
    private boolean isRunning,acceptStopping;
    private int[] currentCols;
    private int playingRow;
    private final Arcade arcade;
    public Stacker(Arcade arcade){
        this.arcade = arcade;
        this.timer = new Timer();
        this.setBackground(Color.BLACK);
        this.setLayout(new GridLayout(ROWS,COLUMNS,3,3));
        stackerBoard = new JPanel[ROWS][COLUMNS];
        for (int x = 0; x < ROWS; x++)
            for (int y = 0; y < COLUMNS; y++){
                stackerBoard[x][y] = new JPanel();
                stackerBoard[x][y].setBackground(BACK_COLOR);
                this.add(stackerBoard[x][y]);
            }
        resetGame();
    }

    /**
     * Reset the stacker game
     */
    private void resetGame(){
        for (int x = 0; x < ROWS; x++)
            for (int y = 0; y < COLUMNS; y++){
                stackerBoard[x][y].setBackground(BACK_COLOR);
            }
        currentCols = new int[]{(COLUMNS/2)-1, (COLUMNS/2), (COLUMNS/2)+1};
        playingRow = ROWS-1;
        stackerBoard[ROWS-1][(COLUMNS/2)].setBackground(BLOCK_COLOR);
        stackerBoard[ROWS-1][(COLUMNS/2)-1].setBackground(BLOCK_COLOR);
        stackerBoard[ROWS-1][(COLUMNS/2)+1].setBackground(BLOCK_COLOR);
        Arcade.updateStatusMessage(GAMEMODE, "Press spacebar to start!");
    }

    /**
     * Start the Pacman game
     */
    private void startGame(){
        resetGame();
        isRunning = true;
        acceptStopping = true;
        runner = new StackerRunner();
        timer.scheduleAtFixedRate(runner, 1,1);
    }

    /**
     * End the game
     */
    private void endGame(){
        runner.cancel();
        isRunning = false;
        Arcade.updateStatusMessage(this.GAMEMODE, "Game over! Play again press spacebar");
    }

    /**
     * Handle the stacker being pressed by space to start game or stop bar
     */
    public void executeSpacePress(){
        if (isRunning) {
            if (acceptStopping)
                runner.stopSquares();
        }else
            startGame();
    }

    /**
     * The stacker running every second
     */
    class StackerRunner extends TimerTask {
        private int direction = 1;
        int delay = ArcadeHelper.STACKER_DELAY;
        int counter = 0;
        private final ArrayList<Integer> tempCols = new ArrayList<>();
        private final ArrayList<Integer> fallingBlockCols = new ArrayList<>();
        @Override
        public void run() {
            counter++;
            if (counter >= delay){
                if (stackerBoard.length == 0)
                    endGame();
                else
                    moveSquare();
                counter = 0;
            }
        }

        /**
         * Move the squares back and forth across the frame
         */
        private void moveSquare(){
            if (currentCols.length == 0) {
                endGame();
                return;
            }
            for (int x : currentCols)
                if (x >= 0 && x < COLUMNS)
                    stackerBoard[playingRow][x].setBackground(BACK_COLOR);
            if (direction == 1){ //Move to the right
                for (int i = 0; i < currentCols.length;i++)
                    currentCols[i] += 1;
                if (currentCols[0] == COLUMNS)
                    direction = -1;
            }else if (direction == -1){
                for (int i = 0; i < currentCols.length;i++)
                    currentCols[i] -= 1;
                if (currentCols[currentCols.length-1] == -1)//Means its off the board
                    direction = 1;
            }
            for (int x : currentCols)
                if (x >= 0 && x < COLUMNS)
                    stackerBoard[playingRow][x].setBackground(BLOCK_COLOR);
        }

        /**
         * Stop the squares when space is pressed
         */
        void stopSquares(){
            delay *= ArcadeHelper.STACKER_MULTIPLIER; //Increases the speed
            if (playingRow == ROWS-1){ //Bottom of stacker
                playingRow--;
                return;
            }

            tempCols.clear();
            fallingBlockCols.clear();
            for (int x : currentCols) {
                if (x >= 0 && x < COLUMNS) {
                    if (stackerBoard[playingRow + 1][x].getBackground() == BLOCK_COLOR) {
                        tempCols.add(x);
                    }else{
                        fallingBlockCols.add(x);
                        stackerBoard[playingRow][x].setBackground(BACK_COLOR);
                    }
                }
            }
            if (fallingBlockCols.size() > 0) {
                counter = (-1)*(ArcadeHelper.STACKER_DELAY*(playingRow+1));
                new Timer().scheduleAtFixedRate(new FallingBlock(playingRow, fallingBlockCols), 1, 1);
            }

            currentCols = tempCols.stream().mapToInt(Integer::intValue).toArray();
            playingRow--;

            if (currentCols.length > 0) {
                if (playingRow == 4) { //Minor Prize
                    runner.pauseCounter();
                    int selection = JOptionPane.showConfirmDialog(null,
                            "Minor Prize: 10$. Continue for Major?", "Continue?",
                            JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
                    if (selection == JOptionPane.YES_OPTION) {
                        runner.resetCounter();
                    } else {
                        Arcade.updateArcadeBalance(ArcadeHelper.STACKER_MINOR_PRIZE);
                        endGame();
                        Arcade.updateStatusMessage(GAMEMODE, "Game over! You have won "+ArcadeHelper.STACKER_MINOR_PRIZE+"$! Space to play again!");
                        return;
                    }
                }
                if (playingRow == 7) { //CHeck and then reduce to 1
                    if (currentCols.length == 2)
                        currentCols = new int[]{currentCols[0]};
                }
                if (playingRow == 10) { //CHeck and then reduce to 2
                    if (currentCols.length == 3)
                        currentCols = new int[]{currentCols[0], currentCols[1]};
                }
                if (playingRow == -1) { //Major Prize
                    endGame();
                    Arcade.updateArcadeBalance(ArcadeHelper.STACKER_MAJOR_PRIZE);
                    Arcade.updateStatusMessage(GAMEMODE, "Congratulations! You have won "+ArcadeHelper.STACKER_MAJOR_PRIZE+"$!");
                }
            }
        }

        /**
         * Reset the counter
         */
        void resetCounter(){counter = 0;}

        /**
         * Pause the counters
         */
        void pauseCounter() {counter = Integer.MIN_VALUE;}
    }

    /**
     * Handle the falling block when you miss the stacker
     */
    class FallingBlock extends TimerTask{
        final ArrayList<Integer> badBlocks;
        int currentRow;
        final int delay = 500;
        int count = 0;
        FallingBlock(int playingRow, ArrayList<Integer> colums){
            badBlocks = colums;
            currentRow = playingRow;
            acceptStopping = false;
        }
        @Override
        public void run() {
            count++;
            if (!isRunning)
                this.cancel();
            if (count == delay){
                count = 0;
                for (int x : badBlocks) {
                    stackerBoard[currentRow][x].setBackground(BACK_COLOR);
                    if (currentRow < ROWS-1)
                        if (stackerBoard[currentRow+1][x].getBackground() == BLOCK_COLOR)
                            endTask();
                        else
                            stackerBoard[currentRow+1][x].setBackground(BLOCK_COLOR);
                }
                currentRow++;
                if (currentRow >= ROWS)
                    endTask();
            }
        }

        /**
         * End the falling block task
         */
        private void endTask(){
            this.cancel();
            acceptStopping = true;
            runner.resetCounter();
        }
    }
}
