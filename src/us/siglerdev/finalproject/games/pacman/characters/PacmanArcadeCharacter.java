package us.siglerdev.finalproject.games.pacman.characters;

import java.util.EnumMap;
import javax.swing.ImageIcon;
import us.siglerdev.finalproject.games.pacman.Pacman;
import us.siglerdev.finalproject.games.pacman.PacmanBoard;
import us.siglerdev.finalproject.games.pacman.utils.ArcadeCharacter;
import us.siglerdev.finalproject.games.pacman.utils.GameDirection;
import us.siglerdev.finalproject.games.pacman.utils.PacmanJPanel;
import us.siglerdev.finalproject.objects.ArcadeJLabel;

public class PacmanArcadeCharacter extends ArcadeCharacter {

    private boolean closed = true;
    private final EnumMap<GameDirection, ImageIcon> pacmanIcons;
    private final ImageIcon flat;
    private int numberOfVisitedFoodSquares;
    public PacmanArcadeCharacter(Pacman pacman){
        super(pacman,false);
        pacmanIcons = new EnumMap<>(GameDirection.class);
        pacmanIcons.put(GameDirection.NORTH, getIconFromPath("Pacman_North"));
        pacmanIcons.put(GameDirection.EAST, getIconFromPath("Pacman_East"));
        pacmanIcons.put(GameDirection.SOUTH, getIconFromPath("Pacman_South"));
        pacmanIcons.put(GameDirection.WEST, getIconFromPath("Pacman_West"));
        flat = getIconFromPath("Pacman_Flat");
    }

    /**
     *
     * @return The number of foods/powerups pacman has visisted
     */
    public int getNumberOfVisitedFoodSquares(){
        return numberOfVisitedFoodSquares;
    }

    @Override
    public ArcadeJLabel getCharacterLabel(){
        characterLabel = new ArcadeJLabel();
        characterLabel.setIcon(closed ? flat : pacmanIcons.get(direction));
        closed = !closed;
        return characterLabel;
    }

    @Override
    public PacmanBoard moveTo(PacmanJPanel panel){
        boolean hasVisisted = panel.hasBeenVisitedByPacman();
        PacmanBoard result = super.moveTo(panel);
        if (!hasVisisted && panel.hasBeenVisitedByPacman()) {
            if (result == PacmanBoard.POWERUP)
                pacman.startPowerup();
            numberOfVisitedFoodSquares++;
        }
        return result;
    }

}
