package us.siglerdev.finalproject.games.pacman.characters;

import java.util.EnumMap;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;
import javax.swing.ImageIcon;
import us.siglerdev.finalproject.games.pacman.Pacman;
import us.siglerdev.finalproject.games.pacman.PacmanBoard;
import us.siglerdev.finalproject.games.pacman.utils.ArcadeCharacter;
import us.siglerdev.finalproject.games.pacman.utils.GameDirection;
import us.siglerdev.finalproject.games.pacman.utils.PacmanJPanel;
import us.siglerdev.finalproject.objects.ArcadeJLabel;

public class GhostArcadeCharacter extends ArcadeCharacter {

    private final EnumMap<GameDirection, ImageIcon> ghostIcons;
    private boolean panicMode,panicColor;
    private final ImageIcon panicBlue;
    private final ImageIcon panicWhite;
    private Timer panicTimer;
    private boolean released;
    private final String colorName;
    public GhostArcadeCharacter(Pacman pacman, String colorName){
        super(pacman,true);
        this.colorName = colorName;
        ghostIcons = new EnumMap<>(GameDirection.class);
        ghostIcons.put(GameDirection.NORTH, getIconFromPath("Ghost_"+colorName+"_North"));
        ghostIcons.put(GameDirection.SOUTH, getIconFromPath("Ghost_"+colorName+"_South"));
        ghostIcons.put(GameDirection.EAST, getIconFromPath("Ghost_"+colorName+"_East"));
        ghostIcons.put(GameDirection.WEST, getIconFromPath("Ghost_"+colorName+"_West"));
        panicBlue = getIconFromPath("Ghost_DBlue_All");
        panicWhite = getIconFromPath("Ghost_White_All");
        panicTimer = new Timer();
        released = false;

    }

    public boolean isReleased(){
        return released;
    }

    public void release(){
        this.moveTo(pacman.pacmanBoard[9][7]);
        released = true;
    }

    public void sendBackToHome(){
        released = false;
        panicMode = false;
        panicColor = false;
        panicTimer.cancel();
        switch (colorName) {
            case "Red":
                this.moveTo(pacman.pacmanBoard[9][10]);
                break;
            case "Blue":
                this.moveTo(pacman.pacmanBoard[10][10]);
                break;
            case "Yellow":
                this.moveTo(pacman.pacmanBoard[10][9]);
                break;
        }
    }

    private static final Random rand = new Random();
    public PacmanBoard moveGhost(){
        //If there are 3 possible directions, pick a random
        //Only choose direction if can't move in same direction anymore
        EnumMap<GameDirection,PacmanJPanel> dirs = new EnumMap<>(GameDirection.class);
        PacmanJPanel temp;
        for (GameDirection gameDirection : GameDirection.values()) {
            temp = canGhostMoveDirection(gameDirection, this.currentSquare.getActualCoords());
            if (temp != null)
                dirs.put(gameDirection, temp);
        }

        boolean isValidMove = false;
        for (GameDirection dir : dirs.keySet())
            if (dir == direction)
                isValidMove = true;

        /* 4 Way Intersection, pick a random direction */
        if (dirs.size() >= 4) {
            this.direction = (GameDirection) dirs.keySet().toArray()[rand.nextInt(dirs.size())];
            return this.moveTo(dirs.get(direction));
        }else if (dirs.size() == 3) { //Most likely gonna continue direction, otherwise choose random
            if (isValidMove) {
                if (rand.nextInt(3) == 1) {
                    this.direction = (GameDirection) dirs.keySet().toArray()[rand.nextInt(dirs.size())];
                    return this.moveTo(dirs.get(direction));
                }else
                    this.moveTo(dirs.get(direction));
            } else{
                this.direction = (GameDirection) dirs.keySet().toArray()[rand.nextInt(dirs.size())];
                return this.moveTo(dirs.get(direction));
            }
        }else if (dirs.size() == 2){ //Try to continue direction if possible
            if (isValidMove)
                this.moveTo(dirs.get(direction));
            else {
                this.direction = (GameDirection) dirs.keySet().toArray()[rand.nextInt(dirs.size())];
                this.moveTo(dirs.get(direction));
            }
        }else{
            return this.moveTo(dirs.get(dirs.keySet().iterator().next())); //Should at least be one
        }
        return null;
    }

    /**
     * Determines if a ghost can move in a certain direction
     * @param dir GameDirection - The direction the ghost is going to go
     * @param coords - the ghost's current positions
     * @return - Null if can't move, The PacmanJPanel if it can
     */
    private PacmanJPanel canGhostMoveDirection(GameDirection dir, int[] coords){
        int x = 0, y = 0;
        switch (dir){
            case NORTH:
                x=-1;
                break;
            case EAST:
                y=1;
                break;
            case WEST:
                y=-1;
                break;
            case SOUTH:
                x=1;
                break;
            default:
                break;
        }
        if (pacman.pacmanBoard[coords[0]+x][coords[1]+y].canGhostVisitSquare()) { //Makes sure not a ghost already in that square
            return pacman.pacmanBoard[coords[0]+x][coords[1]+y];
        }
        return null;
    }

    /**
     * Start the panic mode where ghosts can be eaten
     */
    public void executePanicMode(){
        try{
            panicTimer.cancel();
        }catch (Exception e) {
            System.out.println("Can't cancel panicTimer");
        }
        panicTimer = new Timer();
        panicMode = true;
        panicTimer.scheduleAtFixedRate(new TimerTask() {
            private int counts = 0;
            private int timesExecuted = 10000;
            @Override
            public void run() {
                timesExecuted--;
                if (counts++ < Pacman.GAME_TICK)
                    return;
                counts = 0;
                if (timesExecuted <= 5000)
                    panicColor = !panicColor;
                if (timesExecuted <= 0){
                    this.cancel();
                    panicMode = false;
                    panicColor = false;
                }
            }
        },0,1);
    }

    /**
     *
     * @return Whether the ghost is in panic mode
     */
    public boolean isPanicMode(){
        return panicMode;
    }


    @Override
    public ArcadeJLabel getCharacterLabel() {
        characterLabel = new ArcadeJLabel();
        if (panicMode){
            characterLabel.setIcon(panicColor ? panicWhite : panicBlue);
        }else
            characterLabel.setIcon(ghostIcons.get(direction));
        return characterLabel;
    }
}