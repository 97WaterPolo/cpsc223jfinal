package us.siglerdev.finalproject.games.pacman.utils;

import java.awt.Color;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import us.siglerdev.finalproject.Arcade;
import us.siglerdev.finalproject.games.GameMode;
import us.siglerdev.finalproject.games.pacman.Pacman;
import us.siglerdev.finalproject.games.pacman.PacmanBoard;
import us.siglerdev.finalproject.games.pacman.characters.GhostArcadeCharacter;
import us.siglerdev.finalproject.games.pacman.characters.PacmanArcadeCharacter;
import us.siglerdev.finalproject.objects.ArcadeJLabel;
import us.siglerdev.finalproject.utils.ArcadeHelper;

public class PacmanJPanel extends JPanel {

    private static ImageIcon food,powerup;
    private boolean hasBeenVisitedByPacman;
    private final PacmanBoard squareType;
    private final int[] coords;
    private final ArrayList<ArcadeCharacter> arcadeCharacters;
    private JLabel defaultLabel;
    public PacmanJPanel(PacmanBoard squareType, int[] coords){
        super();
        this.squareType = squareType;
        this.coords = coords;
        this.arcadeCharacters = new ArrayList<>();
        this.setBackground(Color.BLACK);
        try {
            setupPacmanBoard();
        }catch (IOException e) {e.printStackTrace();}
    }

    /**
     * Sets up the Pacman board with food and power up and walls
     * @throws IOException If the image isnt found
     */
    private void setupPacmanBoard() throws IOException {
        defaultLabel = new ArcadeJLabel();
        BufferedImage img;
        if (food == null) {
            img = ImageIO.read(new File("imgs/Food.JPG"));
            Image dimg = img.getScaledInstance(ArcadeHelper.getScaledInstance(),
                    ArcadeHelper.getScaledInstance(),
                    Image.SCALE_SMOOTH);
            food = new ImageIcon(dimg);
        }

        if (powerup == null) {
            img = ImageIO.read(new File("imgs/Powerup.JPG"));
            Image dimg = img.getScaledInstance(ArcadeHelper.getScaledInstance(),
                    ArcadeHelper.getScaledInstance(),
                    Image.SCALE_SMOOTH);
            powerup = new ImageIcon(dimg);
        }


        switch (squareType) {
            case FOOD:
                defaultLabel.setIcon(food);
                break;
            case GHOST: //Home Base
                this.setBackground(Color.GRAY);
                break;
            case GHOST_EXIT:
                this.setBackground(Color.ORANGE);
                break;
            case POWERUP:
                defaultLabel.setIcon(powerup);
                break;
            case WALL:
                this.setBackground(Color.BLUE);
                break;
            default:
                //Starting position
                break;
        }
        this.add(defaultLabel);

    }

    /**
     * Has a character visist this corresponding square
     * @param arcadeCharacter - The character visiting the square
     */
    public void visitSquare(ArcadeCharacter arcadeCharacter){
        arcadeCharacters.add(arcadeCharacter);
        if (arcadeCharacter instanceof PacmanArcadeCharacter){
            PacmanArcadeCharacter pacmanCharacter = (PacmanArcadeCharacter) arcadeCharacter;
            if (!hasBeenVisitedByPacman) {
                hasBeenVisitedByPacman = true;
                Arcade.updateArcadeBalance(20);
                Arcade.updateStatusMessage(GameMode.PAC_MAN, Pacman.lives + " Lives      ", true);
            }
        }
        this.removeAll();
        ArcadeJLabel label = arcadeCharacter.getCharacterLabel();
        this.add(label);
        revalidate();
        repaint();
    }

    /**
     * Forces a character to leave square
     * @param arcadeCharacter the arcade character that is leaving this square
     */
    public void leaveSquare(ArcadeCharacter arcadeCharacter){
        arcadeCharacters.remove(arcadeCharacter);
        this.removeAll();
        if (!hasBeenVisitedByPacman){
            this.add(defaultLabel); //Just removes all the labels
        }
    }

    /**
     * Used to determine if a character can go to a square
     * @return Whether the square is a food, powerup, or empty
     */
    public boolean canCharacterVisitSquare(){
        return squareType == PacmanBoard.POWERUP || squareType == PacmanBoard.FOOD;
    }

    /**
     * Used by Ghosts only to prevent ghost overlaps
     * @return Whether or not a ghost can visit the square
     */
    public boolean canGhostVisitSquare(){
        for(ArcadeCharacter character : arcadeCharacters){
            if (character.isGhost()) {
                return false;
            }
        }
        return canCharacterVisitSquare();
    }

    /**
     *
     * @return Whether there are multiple characters on a given square
     */
    public boolean overlapOfCharacters(){
        return arcadeCharacters.size() > 1;
    }

    /**
     *
     * @return an int[] array of the panel
     */
    public int[] getActualCoords(){
        return coords;
    }

    /**
     *
     * @return whether pacman has visisted the square
     */
    public boolean hasBeenVisitedByPacman(){
        return hasBeenVisitedByPacman;
    }

    /**
     * Gets the ghost at the current square
     * @return Returns the ghost if it is found, null if no ghost
     */
    public GhostArcadeCharacter getGhost(){
        for (ArcadeCharacter character : arcadeCharacters)
            if (character.isGhost())
                return (GhostArcadeCharacter) character;
        return null;
    }

    /**
     *
     * @return The type of square of the panel
     */
    public PacmanBoard getSquareType(){return squareType;}


}
