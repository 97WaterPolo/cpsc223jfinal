package us.siglerdev.finalproject.games.pacman.utils;

/**
 * Enum of all the possible directions
 */
public enum GameDirection {
    NORTH,SOUTH,EAST,WEST
}
