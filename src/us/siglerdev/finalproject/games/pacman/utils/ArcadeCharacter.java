package us.siglerdev.finalproject.games.pacman.utils;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import us.siglerdev.finalproject.games.pacman.Pacman;
import us.siglerdev.finalproject.games.pacman.PacmanBoard;
import us.siglerdev.finalproject.objects.ArcadeJLabel;
import us.siglerdev.finalproject.utils.ArcadeHelper;

public abstract class ArcadeCharacter extends JLabel {
    protected GameDirection direction;
    private final boolean isGhost;
    private GameDirection input;
    protected PacmanJPanel currentSquare;
    protected ArcadeJLabel characterLabel;
    protected final Pacman pacman;

    protected ArcadeCharacter(Pacman pacman, boolean isGhost){
        this.direction = GameDirection.WEST;
        this.input = GameDirection.WEST;
        this.isGhost = isGhost;
        this.currentSquare = null;
        this.pacman = pacman;
    }

    /**
     *
     * @return Characters current direction
     */
    public GameDirection getCurrentDirection() {
        return direction;
    }

    /**
     *
     * @return The characters next "input" direction, PACMAN = keyboard, Ghost = random
     */
    public GameDirection getCurrentInputDirection(){return input;}

    /**
     *
     * @param direction Set the direction of the character
     */
    public void setDirection(GameDirection direction) {
        this.direction = direction;
    }

    /**
     *
     * @param direction set the input direction of character
     */
    public void setInputDirection(GameDirection direction){
        this.input = direction;
    }

    /**
     *
     * @return PacmanJPanel - The current panel the character is at
     */
    public PacmanJPanel getPosition(){return currentSquare;}

    /**
     *
     * @return Is the character a ghost?
     */
    public boolean isGhost(){
        return isGhost;
    }

    /**
     *
     * @param panel The panel the character should move too
     * @return The type of board the character moved to (Powerup, or Food)
     */
    public PacmanBoard moveTo(PacmanJPanel panel){
        if (this.currentSquare != null)
            this.currentSquare.leaveSquare(this);
        if (panel == null){
            return null;
        }
        currentSquare = panel;
        this.currentSquare.visitSquare(this);
        return this.currentSquare.getSquareType();
    }

    /**
     *
     * @param path path to image in the "imgs" folder, used to import new icons
     * @return The image icon to be set to the character
     */
    protected ImageIcon getIconFromPath(String path){
        BufferedImage img;
        try {
            img = ImageIO.read(new File("imgs/"+path+".JPG"));
            Image dimg = img.getScaledInstance(ArcadeHelper.getScaledInstance(),
                    ArcadeHelper.getScaledInstance(),
                    Image.SCALE_SMOOTH);
            return new ImageIcon(dimg);
        }catch (Exception e) {
            System.out.println("Failed finding image for " + path);
        }

        return null;
    }

    /**
     *
     * @return Returns the ArcadeJLabel with the character icon.
     */
    public abstract ArcadeJLabel getCharacterLabel();


}
