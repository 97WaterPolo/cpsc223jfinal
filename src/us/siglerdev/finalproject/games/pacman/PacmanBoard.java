package us.siglerdev.finalproject.games.pacman;

/**
 * The types of boards that each JPanel can be
 */
public enum PacmanBoard {

    WALL('w'), FOOD('f'), GHOST('g'), GHOST_EXIT('e'), STARTING('s'), POWERUP('p');

    private static final char[][] map = {
            {'w','w','w','w','w','w','w','w','w','w','w','w','w','w','w','w','w','w','w','w'},
            {'w','f','f','f','f','f','w','w','f','f','f','w','f','f','f','f','f','f','f','w'},
            {'w','f','w','p','w','f','w','w','f','w','p','w','f','w','w','w','w','w','f','w'},
            {'w','f','w','f','w','f','w','w','f','f','f','w','f','f','f','w','f','p','f','w'},
            {'w','f','w','f','f','f','w','w','f','w','f','w','f','w','f','w','f','w','f','w'},
            {'w','f','f','f','w','f','f','f','f','f','f','f','f','f','f','w','f','w','f','w'},
            {'w','f','w','f','w','f','w','f','w','w','w','w','w','f','w','w','f','w','f','w'},
            {'w','f','w','f','w','f','w','f','f','f','f','f','f','f','f','f','f','w','f','w'},
            {'w','f','w','f','w','f','w','f','w','w','w','w','f','w','f','w','f','f','f','w'},
            {'w','f','w','f','w','f','w','f','e','g','g','w','f','w','f','w','f','w','f','w'},//
            {'w','f','f','f','f','f','f','f','w','g','g','w','f','f','f','f','f','f','f','w'},//
            {'w','w','w','w','w','f','w','f','w','w','w','w','f','w','f','w','f','w','f','w'},
            {'w','f','f','f','f','f','w','f','f','f','f','f','f','w','f','w','f','w','f','w'},
            {'w','f','w','w','f','f','w','f','w','f','w','w','f','w','f','w','f','w','f','w'},
            {'w','f','w','f','f','w','w','f','f','f','f','f','f','w','f','f','f','f','f','w'},
            {'w','f','w','f','w','w','w','f','w','f','w','w','f','w','f','w','w','w','f','w'},
            {'w','f','w','f','f','f','f','f','f','f','w','w','f','w','f','f','f','w','f','w'},
            {'w','f','w','w','f','w','w','w','w','f','w','w','f','w','w','w','f','w','f','w'},
            {'w','p','f','f','f','f','f','f','f','f','f','f','f','f','f','f','f','f','p','w'},
            {'w','w','w','w','w','w','w','w','w','w','w','w','w','w','w','w','w','w','w','w'},
    };
    private final char c;
    PacmanBoard(char c){
        this.c = c;
    }

    /**
     * Obtain the board type from char
     * @param c the char from the map
     * @return the proper pacman board
     */
    private static PacmanBoard getTypeFromChar(char c){
        for (PacmanBoard b : values())
            if (b.c == c)
                return b;
        return null;
    }

    /**
     * Converts coords to Pacman Board type
     * @param x x axis (rows)
     * @param y y axis (columns)
     * @return The Pacman board type
     */
    public static PacmanBoard convertCoordsToChar(int x, int y){
        return getTypeFromChar(map[x][y]);
    }

}
