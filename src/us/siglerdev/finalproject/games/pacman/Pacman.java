package us.siglerdev.finalproject.games.pacman;

import java.awt.Color;
import java.awt.Component;
import java.awt.GridLayout;
import java.util.Timer;
import java.util.TimerTask;
import javax.swing.JPanel;
import us.siglerdev.finalproject.Arcade;
import us.siglerdev.finalproject.games.GameMode;
import us.siglerdev.finalproject.games.pacman.characters.GhostArcadeCharacter;
import us.siglerdev.finalproject.games.pacman.characters.PacmanArcadeCharacter;
import us.siglerdev.finalproject.games.pacman.utils.GameDirection;
import us.siglerdev.finalproject.games.pacman.utils.PacmanJPanel;
import us.siglerdev.finalproject.utils.ArcadeHelper;

public class Pacman extends JPanel {

    public static final int GAME_TICK = 250;
    public final PacmanJPanel[][] pacmanBoard;
    private final Arcade arcade;
    private final Timer timer;
    private final PacmanArcadeCharacter pacman;
    private final GhostArcadeCharacter[] ghosts;
    private final TimerTask pacmanRunner;
    private int numberOfFoodSquares;
    private boolean isRunning;
    public static int lives = 3;
    public Pacman(Arcade arcade) {
        this.arcade = arcade;
        lives = 3;
        this.isRunning = false;
        timer = new Timer();
        this.setBackground(Color.BLACK);
        this.setLayout(new GridLayout(ArcadeHelper.ROWS, ArcadeHelper.COLUMNS, 3, 3));
        pacmanBoard = new PacmanJPanel[ArcadeHelper.ROWS][ArcadeHelper.COLUMNS];

        PacmanBoard temp;
        for (int x = 0; x < ArcadeHelper.ROWS; x++)
            for (int y = 0; y < ArcadeHelper.COLUMNS; y++) {
                temp = PacmanBoard.convertCoordsToChar(x, y);
                if (temp == PacmanBoard.FOOD || temp == PacmanBoard.POWERUP)
                    numberOfFoodSquares++;
                pacmanBoard[x][y] = new PacmanJPanel(temp, new int[]{x, y});
                this.add(pacmanBoard[x][y]);
            }
        ghosts = new GhostArcadeCharacter[]{
                new GhostArcadeCharacter(this,"Red"),
                new GhostArcadeCharacter(this,"Blue"),
                new GhostArcadeCharacter(this,"Yellow")};
        ghosts[0].moveTo(pacmanBoard[9][10]);
        ghosts[1].moveTo(pacmanBoard[10][10]);
        ghosts[2].moveTo(pacmanBoard[10][9]);
        pacman = new PacmanArcadeCharacter(this);
        pacman.moveTo(pacmanBoard[18][10]);

        pacmanRunner = new PacmanRunner();
        GameMode gameMode = GameMode.PAC_MAN;
        Arcade.updateStatusMessage(gameMode, "Press any arrow key to start!");

    }

    /**
     * End the Pacman game
     */
    private void endGame(){
        for (Component c : this.getComponents())
            this.remove(c);
        pacmanRunner.cancel();
        arcade.endGame();
    }

    /**
     * Starts Pacman
     */
    private void startGame(){
        isRunning = true;
        timer.scheduleAtFixedRate(pacmanRunner, 0, 1);
    }

    /**
     * Change the direction of Pacman (input)
     * @param direction the direction to change the input
     */
    public void updatePacmanDirection(GameDirection direction){
        pacman.setInputDirection(direction);
        if (!isRunning)
            startGame();

    }

    /**
     * Starts the Pacman powerup by turning all ghosts into panic mode
     */
    public void startPowerup(){
        for (GhostArcadeCharacter ghost : ghosts){
            if (ghost.isReleased())
                ghost.executePanicMode();
        }
    }

    /**
     * Send all the ghosts bac to home
     */
    private void resetGhosts(){
        for (GhostArcadeCharacter ghostArcadeCharacter : ghosts)
            ghostArcadeCharacter.sendBackToHome();
    }

    /**
     * Every tick (1ms) run the timer checks
     */
    class PacmanRunner extends TimerTask{
        private PacmanJPanel pacmanJPanel;
        int count = 0;
        int ghostReleaseCount = 7000;
        @Override
        public void run() {
            ghostReleaseCount++;
            if (count++ < GAME_TICK)
                return;
            count = 0;

            /* Start of Ghost Code */
            for (GhostArcadeCharacter ghost : ghosts){
                if (ghost.isReleased()){
                    ghost.moveGhost();
                }
            }

            if (ghostReleaseCount >= 10000){
                ghostReleaseCount = 0;
                for (GhostArcadeCharacter ghostArcadeCharacter : ghosts)
                    if (!ghostArcadeCharacter.isReleased()){
                        ghostArcadeCharacter.release();
                        break;
                    }
            }
            /* End of Ghost Code */

            overlapCheck();

            /* Start of Pacman Code */
            pacmanJPanel = canCharacterMoveDirection(pacman.getCurrentInputDirection(), pacman.getPosition().getActualCoords());
            if (pacmanJPanel != null){ //Means they can change directions
                pacman.setDirection(pacman.getCurrentInputDirection());
                pacman.moveTo(pacmanJPanel);
            }else{
                pacmanJPanel = canCharacterMoveDirection(pacman.getCurrentDirection(), pacman.getPosition().getActualCoords());
                if (pacmanJPanel != null)
                    pacman.moveTo(pacmanJPanel);
            }

            if (pacman.getNumberOfVisitedFoodSquares() == numberOfFoodSquares){
                Arcade.updateArcadeBalance(5000);
                endGame();
            }
            /* End of Pacman Code */
            overlapCheck();

            revalidate(); //Revalidate the JFrame
            repaint(); //Repaint the jframe
        }

    }

    /**
     * What happens when there are an overlap of characters. If it is powerup mode
     * then kill ghost, otherwise kill pacman
     * @return
     */
    private boolean overlapCheck(){
        if (pacman.getPosition().overlapOfCharacters()){
            if (pacman.getPosition().getGhost() != null) {
                GhostArcadeCharacter ghost = pacman.getPosition().getGhost();
                if (ghost.isPanicMode()){
                    ghost.sendBackToHome();
                    Arcade.updateArcadeBalance(200);
                }else {
                    lives--;
                    pacman.setDirection(GameDirection.EAST);
                    pacman.moveTo(pacmanBoard[18][10]);
                    resetGhosts();
                    if (lives == 0) {
                        endGame();
                    }
                    Arcade.updateStatusMessage(GameMode.PAC_MAN, Pacman.lives + " Lives      ", true);
                }
                return true;
            }else{
                System.out.println("FIND ME THE GHOST IS NULL IDK HOW");
            }
        }
        return false;
    }

    /**
     * Whether the character can move a direction
     * @param direction The direction attempting to move in
     * @param coords Coords of the character
     * @return PacmanJPanel if it can move, Null otherwise
     */
    private PacmanJPanel canCharacterMoveDirection(GameDirection direction, int[] coords){
        int x = 0, y = 0;
        switch (direction){
            case NORTH:
                x=-1;
                break;
            case EAST:
                y=1;
                break;
            case WEST:
                y=-1;
                break;
            case SOUTH:
                x=1;
                break;
        }
        if (pacmanBoard[coords[0]+x][coords[1]+y].canCharacterVisitSquare()) {
            return pacmanBoard[coords[0]+x][coords[1]+y];
        }
        return null;
    }

}
