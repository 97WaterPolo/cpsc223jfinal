package us.siglerdev.finalproject.games;

/* Enum of all the valid games and their proper names */
public enum GameMode {
    STACKER("Stacker"), PAC_MAN("Pac-Man"), STANDBY("Home");
    public final String name;
    GameMode(String name){
        this.name = name;
    }
}
