package us.siglerdev.finalproject;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.Timer;
import java.util.TimerTask;
import javax.swing.JMenuItem;
import us.siglerdev.finalproject.games.GameMode;
import us.siglerdev.finalproject.games.pacman.Pacman;
import us.siglerdev.finalproject.games.pacman.utils.GameDirection;
import us.siglerdev.finalproject.games.stacker.Stacker;
import us.siglerdev.finalproject.objects.ArcadeJLabel;
import us.siglerdev.finalproject.objects.ArcadeJMenuItem;
import us.siglerdev.finalproject.utils.ArcadeHelper;
import us.siglerdev.finalproject.objects.ArcadeJMenuBar;


import javax.swing.JFrame;
import javax.swing.JPanel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

public class Arcade extends JFrame implements ActionListener, ItemListener, KeyListener {

    private GameMode GAMEMODE = GameMode.STANDBY;
    private static ArcadeJLabel statusLabel;
    private Pacman pacman;
    private Stacker stacker = null;
    private final BorderLayout layout = new BorderLayout();
    private static int balance;
    public Arcade(){
        super("Xander's Arcade");
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.setSize(ArcadeHelper.HOME_WIDTH,ArcadeHelper.HOME_HEIGHT);
        this.setLayout(layout);
        this.setBackground(Color.WHITE);

        statusLabel = new ArcadeJLabel("This is the default status bar");
        JPanel status = new JPanel();
        status.add(statusLabel);
        this.add(status, BorderLayout.NORTH);
        this.setJMenuBar(new ArcadeJMenuBar(this));
        addListeners();
    }

    /**
     * End whatever game has is being started
     */
    public void endGame(){
        this.GAMEMODE = GameMode.STANDBY;
        this.setSize(ArcadeHelper.HOME_WIDTH,ArcadeHelper.HOME_HEIGHT);
        if (layout.getLayoutComponent(BorderLayout.CENTER) != null)
            this.remove(layout.getLayoutComponent(BorderLayout.CENTER));
        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                updateStatusMessage(GAMEMODE, "You currently have winnings of: $" + balance);
            }
        },200);
    }

    /**
     * Initialize and start the stacker game
     */
    public void startStacker(){
        if (layout.getLayoutComponent(BorderLayout.CENTER) != null)
            this.remove(layout.getLayoutComponent(BorderLayout.CENTER));
        GAMEMODE = GameMode.STACKER;
        this.setSize(ArcadeHelper.STACKER_WIDTH,ArcadeHelper.STACKER_HEIGHT);
        stacker = new Stacker(this);
        this.add(stacker, BorderLayout.CENTER);
    }

    /**
     * Initialize and start the pacman game
     */
    public void startPacman(){
        if (layout.getLayoutComponent(BorderLayout.CENTER) != null)
            this.remove(layout.getLayoutComponent(BorderLayout.CENTER));
        GAMEMODE = GameMode.PAC_MAN;
        this.setSize(ArcadeHelper.PACMAN_WIDTH ,ArcadeHelper.PACMAN_HEIGHT);
        pacman = new Pacman(this);
        this.add(pacman, BorderLayout.CENTER);
    }

    /**
     * Update your balance
     * @param amt amt to change the balance
     */
    public static void updateArcadeBalance(int amt) { balance += amt;}

    /**
     * Change the status message in the frame
     * @param gameMode the gamemode sending the message
     * @param msg The msg
     */
    public static void updateStatusMessage(GameMode gameMode, String msg){
        updateStatusMessage(gameMode,msg,false);
    }

    /**
     * Change the status message in the frame
     * @param gameMode the gamemode sending the message
     * @param msg The msg
     * @param bringBalance Whether too append balance at the end.
     */
    public static void updateStatusMessage(GameMode gameMode, String msg, boolean bringBalance){
        statusLabel.setText(gameMode.name + ": " + msg + (bringBalance ? "$"+balance : ""));
    }

    /**
     * For every component in the frame, add this as a action listener or key listener
     */
    private void addListeners(){
        for (Component c : ArcadeHelper.getAllComponents(this.getRootPane())){
            if (c instanceof ArcadeJMenuItem) {
                ((JMenuItem) c).addActionListener(this);
            }
        }
        this.addKeyListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        Object src = e.getSource();
        if (src instanceof ArcadeJMenuItem)
            ((ArcadeJMenuItem)src).executeMenuItem();

    }

    @Override
    public void itemStateChanged(ItemEvent e) {

    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {
        int key = e.getKeyCode();
        if (GAMEMODE == GameMode.STACKER && key == KeyEvent.VK_SPACE){
            stacker.executeSpacePress();
        }
        if (GAMEMODE == GameMode.PAC_MAN){
            if (key == KeyEvent.VK_UP)
                pacman.updatePacmanDirection(GameDirection.NORTH);
            else if(key == KeyEvent.VK_DOWN)
                pacman.updatePacmanDirection(GameDirection.SOUTH);
            else if (key == KeyEvent.VK_RIGHT)
                pacman.updatePacmanDirection(GameDirection.EAST);
            else if (key == KeyEvent.VK_LEFT)
                pacman.updatePacmanDirection(GameDirection.WEST);
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {

    }
}
