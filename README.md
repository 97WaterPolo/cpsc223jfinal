# Xander's Arcade

Simple arcade that has Pacman and Stacker

## What for?

This is for Computer Science 223J at Cal State Fullerton, this project is by Alexander Sigler

## How to use
Run the program and use the Games Menu Bar to select which game to play.

[Javadocs can be found here](http://siglerdev.us/cpsc223j)
